use rand::Rng;

mod args;
mod chars;

fn main() {
    use args::Arguments;
    use clap::Parser;

    let args = Arguments::parse();
    let customchars: Vec<char> = args.custom.chars().collect();
    let newstring: NewString = {NewString {length: args.length, numbers: args.numbers, uppercase: args.uppercase, symbols: args.symbols, lowercase: args.lowercase, customcharset: customchars, output: args.output}};
    let generatedstring: String = generate_string(newstring);
    if !args.output {println!("{}", generatedstring);}
}

struct NewString {
    length: u64,
    numbers: bool,
    uppercase: bool,
    lowercase: bool,
    symbols: bool,
    customcharset: Vec<char>,
    output: bool
}

fn generate_string(newstring: NewString) -> String {
    let mut returnstring = "".to_string();
    let charset: Vec<char> = chars::get_charset(newstring.lowercase, newstring.uppercase, newstring.numbers, newstring.symbols, newstring.customcharset);

    if charset.is_empty() {
        println!("Your charset is empty, please use -h or --help to get help for using this program.");
        std::process::exit(1);
    }

    if !newstring.output {
        for _ in 0..newstring.length {
            returnstring.push(charset[generate_random_number(0, charset.len())]);
        }
    }else {
        for _ in 0..newstring.length {
            print!("{}", charset[generate_random_number(0, charset.len())]);
        }
    }

    returnstring
}

fn generate_random_number(min: usize, max: usize) -> usize {
    rand::thread_rng().gen_range(min..max) as usize
}