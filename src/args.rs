use clap::Parser;

/// A Simple String Generator
#[derive(Parser, Debug, Default)]
#[clap(author = "furkan.gnu on GitLab/GitHub", version, about)]
pub struct Arguments {
  /// Length of the string
  pub length: u64,
  
  #[clap(short, long)]
  /// Enable numbers
  pub numbers: bool,

  #[clap(short, long)]
  /// Enable uppercase letters
  pub uppercase: bool,

  #[clap(short, long)]
  /// Enable lowercase letters
  pub lowercase: bool,

  #[clap(short, long)]
  /// Enable symbols
  pub symbols: bool,

  #[clap(default_value = "", short, long)]
  /// Add custom characters or strings
  pub custom: String,

  #[clap(short, long)]
  /// Output char by char (default outputs the whole string after creation)
  pub output: bool
}