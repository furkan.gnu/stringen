pub fn get_charset(lowercase: bool, uppercase: bool, numbers: bool, symbols: bool, mut customcharset: Vec<char>) -> Vec<char> {
  let mut returnvector: Vec<char> = vec![];
  let mut lowercaseletters: Vec<char> = vec![];
  let mut uppercaseletters: Vec<char> = vec![];
  let mut numberletters: Vec<char> = vec![];
  let mut symbolletters: Vec<char> = vec![];

  for c in 'a'..='z' {lowercaseletters.push(c);}
  for c in 'A'..='Z' {uppercaseletters.push(c);}
  for c in '0'..='9' {numberletters.push(c);}
  for c in 33 as u8..=47 as u8 {symbolletters.push(c as char);}
  for c in 58 as u8..=64 as u8 {symbolletters.push(c as char);}
  for c in 91 as u8..=96 as u8 {symbolletters.push(c as char);}
  for c in 123 as u8..=126 as u8 {symbolletters.push(c as char);}
  
  if lowercase {returnvector.append(&mut lowercaseletters);}
  if uppercase {returnvector.append(&mut uppercaseletters);}
  if numbers {returnvector.append(&mut numberletters);}
  if symbols {returnvector.append(&mut symbolletters);}
  if !customcharset.is_empty() {returnvector.append(&mut customcharset);}
  
  returnvector
}